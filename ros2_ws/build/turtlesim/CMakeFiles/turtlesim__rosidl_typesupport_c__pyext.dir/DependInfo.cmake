# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/nr/ros2_ws/build/turtlesim/rosidl_generator_py/turtlesim/_turtlesim_s.ep.rosidl_typesupport_c.c" "/home/nr/ros2_ws/build/turtlesim/CMakeFiles/turtlesim__rosidl_typesupport_c__pyext.dir/rosidl_generator_py/turtlesim/_turtlesim_s.ep.rosidl_typesupport_c.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "RCUTILS_ENABLE_FAULT_INJECTION"
  "ROS_PACKAGE_NAME=\"turtlesim\""
  "turtlesim__rosidl_typesupport_c__pyext_EXPORTS"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/nr/ros2_ws/src/ros_tutorials/turtlesim/include"
  "/usr/include/x86_64-linux-gnu/qt5"
  "/usr/include/x86_64-linux-gnu/qt5/QtWidgets"
  "/usr/include/x86_64-linux-gnu/qt5/QtGui"
  "/usr/include/x86_64-linux-gnu/qt5/QtCore"
  "/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++"
  "rosidl_generator_c"
  "rosidl_generator_py"
  "/usr/include/python3.8"
  "/home/nr/ros2_humble/install/rosidl_typesupport_c/include/rosidl_typesupport_c"
  "/home/nr/ros2_humble/install/rosidl_runtime_c/include/rosidl_runtime_c"
  "/home/nr/ros2_humble/install/rosidl_typesupport_interface/include/rosidl_typesupport_interface"
  "/home/nr/ros2_humble/install/action_msgs/include/action_msgs"
  "/home/nr/ros2_humble/install/builtin_interfaces/include/builtin_interfaces"
  "/home/nr/ros2_humble/install/unique_identifier_msgs/include/unique_identifier_msgs"
  "/home/nr/ros2_humble/install/rmw/include/rmw"
  "/home/nr/ros2_humble/install/rcutils/include/rcutils"
  "/home/nr/ros2_humble/install/fastcdr/include"
  "/home/nr/ros2_humble/install/rosidl_runtime_cpp/include/rosidl_runtime_cpp"
  "/home/nr/ros2_humble/install/rosidl_typesupport_fastrtps_cpp/include/rosidl_typesupport_fastrtps_cpp"
  "/home/nr/ros2_humble/install/rosidl_typesupport_fastrtps_c/include/rosidl_typesupport_fastrtps_c"
  "/home/nr/ros2_humble/install/rosidl_typesupport_introspection_c/include/rosidl_typesupport_introspection_c"
  "/home/nr/ros2_humble/install/rosidl_typesupport_introspection_cpp/include/rosidl_typesupport_introspection_cpp"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/nr/ros2_ws/build/turtlesim/CMakeFiles/turtlesim__rosidl_generator_py.dir/DependInfo.cmake"
  "/home/nr/ros2_ws/build/turtlesim/CMakeFiles/turtlesim__rosidl_typesupport_c.dir/DependInfo.cmake"
  "/home/nr/ros2_ws/build/turtlesim/CMakeFiles/turtlesim__rosidl_generator_c.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
